//Classe qui décrit notre objet principal
public class MainObject {

	private String state;
	private int year;
	private float birth_rate;
	private int population;
	
	public MainObject(String state, int year, float birth_rate, int population) {
		super();
		this.state = state;
		this.year = year;
		this.birth_rate = birth_rate;
		this.population = population;
	}
		
	public MainObject() {
		super();
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public float getBirth_rate() {
		return birth_rate;
	}
	public void setBirth_rate(float birth_rate) {
		this.birth_rate = birth_rate;
	}
	public int getPopulation() {
		return population;
	}
	public void setPopulation(int population) {
		this.population = population;
	}

	@Override
	public String toString() {
		return "MainObject [state=" + state + ", year=" + year + ", birth_rate=" + birth_rate + ", population="
				+ population + "]";
	}	
}
