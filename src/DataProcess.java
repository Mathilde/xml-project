import java.util.ArrayList;
import java.util.List;

// Classe qui permet  de fusionner les données des deux fichiers d'entrées
public class DataProcess {

	private List<MainObject> objects;	
	private List<MainObject> finalObjects = null;
	private List<String> statesList = null;
	private List<Integer> yearsList = null;
	
	public DataProcess(List<MainObject> objects) {
		this.objects = objects;
		this.finalObjects = new ArrayList<MainObject>();
		this.statesList = new ArrayList<String>();
		this.yearsList = new ArrayList<Integer>();
		fillList();
		process();
	}
	
	// Méthode qui complète les listes de state et de year
	public void fillList() {
		for (MainObject mainObject : objects) {
			if(!statesList.contains(mainObject.getState())) {
				statesList.add(mainObject.getState());
			}
			if(!yearsList.contains(mainObject.getYear())) {
				yearsList.add(mainObject.getYear());
			}
		}
	}

	// Méthode qui ajoute dans la liste des objets les données du fichier XML si la population est > 0
	public void process() {
		float average = 0;
		int population = 0;
		for (String stateElement : statesList) {
			for (Integer yearElement : yearsList) {
				average = calculMoyenne(stateElement, yearElement); 
				population = getPopulationByState(stateElement);
				if(population > 0) {
					finalObjects.add(new MainObject(stateElement, yearElement, average, population));
				}
			}			
		}
	}	
	
	// Méthode qui calcul la moyenne de naissance des county
	public float calculMoyenne(String state, int year) {
		float total = 0;
		int nb = 0;
		for (MainObject mainObject : objects) {
			if(mainObject.getState().equalsIgnoreCase(state) && mainObject.getYear() == year) {
				nb++;
				total += mainObject.getBirth_rate();
			}
		}
		return total / nb;
	}
	
	// Méthode qui retourne la population en fonction du state passé en paramètre
	public int getPopulationByState(String state) {
		for (MainObject mainObject : objects) {
			if(mainObject.getState().equalsIgnoreCase(state)) {
				return mainObject.getPopulation();
			}
		}
		return 0;
	}

	public List<MainObject> getFinalObjects() {
		return finalObjects;
	}

	public void setFinalObjects(List<MainObject> finalObjects) {
		this.finalObjects = finalObjects;
	}		
}
