import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

// Classe qui permet de générer le fichier xml
public class DataToXml {

	private static final String FILE_NAME_XML_GENERATED = "data.xml";
	private List<MainObject> finalObjects = null;

	public DataToXml(List<MainObject> finalObjects) {
		this.finalObjects = finalObjects;
		process();
	}
	
	// Méthode principale qui génére le fichier XML
	public void process() {
		try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element root = doc.createElement("dataExtracted");
            doc.appendChild(root);
            
            String statePrec = "";
            Element state = null;

            for (int i = 0; i < finalObjects.size(); i++) {

            	if(statePrec == "" || !statePrec.equalsIgnoreCase(finalObjects.get(i).getState())) {
                    state = doc.createElement("state");  
                    Attr name = doc.createAttribute("name");
                    name.setTextContent(String.valueOf(finalObjects.get(i).getState()));
                    state.setAttributeNode(name);
                    root.appendChild(state);                    
            	}

                Attr population = doc.createAttribute("population");
                population.setTextContent(String.valueOf(finalObjects.get(i).getPopulation()));
                state.setAttributeNode(population);

                Element year = doc.createElement("year");

                Attr value = doc.createAttribute("value");
                value.setTextContent(String.valueOf(finalObjects.get(i).getYear()));
                year.setAttributeNode(value);
                state.appendChild(year);

                Element average = doc.createElement("averageBirthRate");
                average.appendChild(doc.createTextNode(String.valueOf(finalObjects.get(i).getBirth_rate())));
                year.appendChild(average);

                statePrec = finalObjects.get(i).getState();
            }

            //write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result =  new StreamResult(new File(FILE_NAME_XML_GENERATED));
            transformer.transform(source, result);

            System.out.println("Le fichier XML a été généré avec succès");

        } catch(ParserConfigurationException pce) {
        	pce.printStackTrace();
        } catch(TransformerException tfe) {
        	tfe.printStackTrace();
        }
	}
}
