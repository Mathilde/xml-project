import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

// Classe principale du projet
public class Main {
	private static final String FILE_NAME_ROWS = "rows.xml";
	private static final String FILE_NAME_STATES = "us-states.xml";
	
	public static void main( String[] args ) throws FileNotFoundException, ParserConfigurationException, SAXException, IOException {
        DataHandler datahandler = new DataHandler();
        datahandler.parse(FILE_NAME_ROWS);
        PopulationHandler populationhandler = new PopulationHandler(datahandler.getObjects());
        populationhandler.parse(FILE_NAME_STATES);
        
        DataProcess dataProcess = new DataProcess(datahandler.getObjects());

        new DataToXml(dataProcess.getFinalObjects());
        
        new XmlValidator().validator();
        
        new XmlToHtml().writeHTML(); 
        
	}
}
