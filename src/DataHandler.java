import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

// Classe qui permet de parser le fichier rows.xml
public class DataHandler extends DefaultHandler {

   /**
    * Un tableau qui contient la hierarchie courante des balises
    * Permet de gérer les balises imbriquées		
    **/
   private List<MainObject> objects;
   private MainObject obj = null;
   private boolean isYearElement = false;
   private boolean isStateElement = false;
   private boolean isBirthRateElement = false;
   
   private final String LOCAL_NAME_ROWS = "rows";
   private final String LOCAL_NAME_YEAR = "year";
   private final String LOCAL_NAME_STATE = "state";
   private final String LOCAL_NAME_BIRTH_RATE = "birth_rate";

   public void parse(InputStream input) throws
           ParserConfigurationException,
           SAXException,IOException
           	{
        // creation du parser SAX
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setValidating(true);
        factory.setNamespaceAware(true);
        SAXParser parser = factory.newSAXParser();
        // lancement du parsing en précisant le flux et le "handler"
        // l'instance qui implémente les méthodes appelées par le parser
        // dans notre cas: this
        parser.parse(input, this);
    }

   public void parse(String filename) throws
           FileNotFoundException,
           ParserConfigurationException,
           SAXException,
           IOException{
        parse(new FileInputStream(filename));
    }

   public void startDocument() throws SAXException{
      // initialisation
	   objects = new ArrayList<MainObject>();	   
   }

   
   public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	   	if(LOCAL_NAME_ROWS.equalsIgnoreCase(localName)) {
	   		obj = new MainObject();
	   	}
        if (LOCAL_NAME_YEAR.equalsIgnoreCase(localName)) {
        	isYearElement = true;
        }
        if (LOCAL_NAME_STATE.equalsIgnoreCase(localName)) {
        	isStateElement = true;
        }
        if (LOCAL_NAME_BIRTH_RATE.equalsIgnoreCase(localName)) {
        	isBirthRateElement = true;
        }
   }
   
   // Méthode qui set notre objet avec les données du fichier XML
   public void characters(char ch[],int start,int length) {    
	   if(isYearElement) {
		   String year = new String(ch,start,length);
		   obj.setYear(Integer.parseInt(year));
	   }
	   if(isStateElement) {
		   String state = new String(ch,start,length);
		   obj.setState(state);
	   }
	   if(isBirthRateElement) {
		   String birthRate = new String(ch,start,length);
		   obj.setBirth_rate(Float.parseFloat(birthRate));
	   }
   } 
   
   public void endElement(String uri, String localName, String qName) throws SAXException {
        if (LOCAL_NAME_YEAR.equalsIgnoreCase(localName)) {
        	isYearElement = false;
        } 
        if (LOCAL_NAME_STATE.equalsIgnoreCase(localName)) {
        	isStateElement = false;
        } 
        if (LOCAL_NAME_BIRTH_RATE.equalsIgnoreCase(localName)) {
        	isBirthRateElement = false;
        } 
	   	if(LOCAL_NAME_ROWS.equalsIgnoreCase(localName)) {
	   		objects.add(obj);
	   	}
    }
   

	public List<MainObject> getObjects() {
		return objects;
	}
	
	public void setObjects(List<MainObject> objects) {
		this.objects = objects;
	}
	
	@Override
	public String toString() {
		String res = "";
		for (MainObject mainObject : objects) {
			res += mainObject;
		}
		return res;
	}	   

}