import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

//Classe qui permet de parser le fichier us-states.xml
public class PopulationHandler extends DefaultHandler {

	private List<MainObject> objects;
	private final String LOCAL_NAME_STATE = "state";
	private final String VALUE_NAME = "name";
	private final String VALUE_POPULATION = "population";
	   
	public PopulationHandler(List<MainObject> objects) {
		this.objects = objects;
	}
	
   public void parse(InputStream input) throws
           ParserConfigurationException,
           SAXException,IOException
           	{
        // creation du parser SAX
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setValidating(true);
        factory.setNamespaceAware(true);
        SAXParser parser = factory.newSAXParser();
        // lancement du parsing en précisant le flux et le "handler"
        // l'instance qui implémente les méthodes appelées par le parser
        // dans notre cas: this
        parser.parse(input, this);
    }

   public void parse(String filename) throws
           FileNotFoundException,
           ParserConfigurationException,
           SAXException,
           IOException{
        parse(new FileInputStream(filename));
    }

   // Méthode qui ajoute la population dans les objets déjà créés
   public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(LOCAL_NAME_STATE.equalsIgnoreCase(localName)) {
            for (MainObject mainObject : objects) {
				if(mainObject.getState().equalsIgnoreCase(attributes.getValue(VALUE_NAME))) {
					mainObject.setPopulation(Integer.parseInt(attributes.getValue(VALUE_POPULATION)));
				}
            }
        }
   }

	public List<MainObject> getObjects() {
		return objects;
	}
	
	public void setObjects(List<MainObject> objects) {
		this.objects = objects;
	}
}