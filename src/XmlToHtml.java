import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

// Classe qui converti le fichier XML en fichier HTML
public class XmlToHtml {
	private static final String FILE_NAME_XML_GENERATED = "data.xml";
	private static final String FILE_NAME_HTML_GENERATED = "data.html";
	private static final String FILE_NAME_XSL = "data.xsl";
	
	// Méthode qui génère le fichier HTML
	public void writeHTML() {	   
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			//lecture de notre document XML
			DocumentBuilder builder = factory.newDocumentBuilder();
			File fileXML = new File(FILE_NAME_XML_GENERATED);
			builder.parse(fileXML);

			//définition du fichier XSL
			new File(FILE_NAME_XSL);

			//Le fichier où nous allons enregistrer le résultat
			StreamResult HTML = new StreamResult(FILE_NAME_HTML_GENERATED);

			//On affiche le résultat HTML dans notre Fenetre
			new Fenetre(FILE_NAME_HTML_GENERATED);

		} catch (DOMException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}       
	}
}
