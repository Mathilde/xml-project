import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

// Classe qui valide le fichier XML généré avec la XSD définie
public class XmlValidator {
	private static final String FILE_NAME_XML_GENERATED = "data.xml";
	private static final String FILE_NAME_XSD = "data.xsd";
	
	// Méthode qui valide le fichier XML
	public void validator() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();		
	    try {
	    	// La validation se fait via un fichier XSD
	        SchemaFactory sfactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	
	        //On créé notre schéma XSD
	        Schema schema = sfactory.newSchema(new File(FILE_NAME_XSD));
	        //On l'affecte à notre factory afin que le document prenne en compte le fichier XSD
	        factory.setSchema(schema);    
	       
	        DocumentBuilder builder = factory.newDocumentBuilder();         
	
	        //création de notre objet d'erreurs
	        ErrorHandler errHandler = new SimpleErrorHandler();
	        //Affectation de notre objet au document pour interception des erreurs éventuelles
	        builder.setErrorHandler(errHandler);
	       
	        File fileXML = new File(FILE_NAME_XML_GENERATED);
	       
	        //On rajoute un bloc de capture pour intercepter les erreurs au cas où il y en ait
	        try {	
	        	builder.parse(fileXML);	
	        	System.out.println("Le fichier data.xml est validé par le fichier data.xsd");
	        } catch (SAXParseException e) {} 
	    } catch (ParserConfigurationException e) {
	       e.printStackTrace();
	    } catch (SAXException e) {
	       e.printStackTrace();
	    } catch (IOException e) {
	       e.printStackTrace();
	    }  
	}
}
