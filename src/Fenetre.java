import java.awt.BorderLayout;
import java.awt.Container;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.text.html.HTMLEditorKit;

// Classe qui permet d'afficher la fenetre dans laquelle est ouvert le fichier HTML
public class Fenetre extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JEditorPane apercu;
	private File htmlFile; 
  
	public Fenetre(String fileName) {

		this.setSize(1000, 1000);
		this.setTitle("Affichage du résultat");
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.htmlFile = new File(fileName);    
		this.init();
		this.setVisible(true);

	    // Ajout de la scroll bar
	    Container c = getContentPane();
	    JScrollPane scroll = new JScrollPane( c );
	    setContentPane( scroll );
	}   

	private void init() {
		apercu = new JEditorPane();
		try {
			apercu.setEditorKit(new HTMLEditorKit());
			apercu.setPage(this.htmlFile.toURI().toURL());
			apercu.setEditable(false);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(apercu, BorderLayout.CENTER);
	}
}
