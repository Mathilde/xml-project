<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h1 style="align:center">XML Project</h1>
  
  
  
  <h3><b>Taux de naissance aux USA</b></h3>
  
	 
    <xsl:for-each select="dataExtracted/state">
		<h4>State : <xsl:value-of select="@name"/> (<xsl:value-of select="@population"/> habitants)	</h4>
		<ul>
			
    		<xsl:for-each select="year">
			<li>Année : <xsl:value-of select="@value"/></li>
				<ul>
				<li>Nombre moyenne de naissance pour 1000 habitants: <xsl:value-of select="averageBirthRate"/></li>
				</ul>
				
    		</xsl:for-each>
		</ul>
	
    </xsl:for-each>
	
  </body>
  </html>
</xsl:template>

</xsl:stylesheet> 